from django.db import models


class Phone(models.Model):
    OS_CHOICES = (("Android", "Android"),
                  ("iOS", "iOS"))
    MEMORY_CHOICES = (("16", "16 GB"),
                      ("32", "32 GB"),
                      ("64", "64 GB"),
                      ("128", "128 GB"))
    RAM_CHOICES = (("2", "2 GB"),
                   ("3", "3 GB"))
    CAMERA_CHOICES = ((5, "5 MP"),
                      (7, "7 MP"),
                      (12, "12 MP"))
    DISPLAY_CHOICES = (("5.5", '5.5"'),
                       ("4.5", '4.5"'),
                       ("5.0", '5"'))

    name = models.CharField(max_length=150)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    in_stock = models.BooleanField()
    ram = models.CharField(max_length=5, choices=RAM_CHOICES)
    display = models.CharField(max_length=5, choices=DISPLAY_CHOICES)
    os = models.CharField(max_length=10)
    memory = models.CharField(max_length=5, choices=MEMORY_CHOICES)
    gps = models.BooleanField()
    camera = models.PositiveIntegerField(choices=CAMERA_CHOICES)

    def __str__(self):
        return self.name