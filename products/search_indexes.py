from haystack import indexes
from products.models import Phone


class PhoneIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name')
    ram = indexes.CharField(model_attr='ram')
    price = indexes.IntegerField(model_attr='price')
    os = indexes.CharField(model_attr='os')
    gps = indexes.BooleanField(model_attr='gps')
    camera = indexes.IntegerField(model_attr='camera')
    memory = indexes.CharField(model_attr='memory')
    display = indexes.CharField(model_attr='display')
    auto = indexes.EdgeNgramField(model_attr='name')


    def get_model(self):
        return Phone

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(in_stock=True)