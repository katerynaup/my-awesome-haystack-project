from django import forms
from haystack.query import SQ
from haystack.query import SearchQuerySet, EmptySearchQuerySet


class FilterForm(forms.Form):

    OS_CHOICES = (("Android", "Android"),
                  ("iOS", "iOS"))
    RAM_CHOICES = (("2", "2 GB"),
                   ("3", "3 GB"))
    MEMORY_CHOICES = (("16", "16 GB"),
                      ("32", "32 GB"),
                      ("64", "64 GB"),
                      ("128", "128 GB"))
    CAMERA_CHOICES = ((5, "5 MP"),
                      (7, "7 MP"),
                      (12, "12 MP"))
    DISPLAY_CHOICES = (("5.5", '5.5"'),
                       ("4.5", '4.5"'),
                       ("5.0", '5"'))
    name = forms.CharField(required=False, max_length=150)
    price = forms.IntegerField(widget=forms.NumberInput(attrs={"type": "range", "min": 0, "max": 40000}), required=False)
    ram = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=RAM_CHOICES, required=False)
    display = forms.MultipleChoiceField(required=False, choices=DISPLAY_CHOICES, widget=forms.CheckboxSelectMultiple)
    os = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=OS_CHOICES, required=False)
    memory = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=MEMORY_CHOICES, required=False)
    gps = forms.BooleanField(required=False)
    camera = forms.MultipleChoiceField(required=False, widget=forms.CheckboxSelectMultiple, choices=CAMERA_CHOICES)

    def __init__(self, *args, **kwargs):
        self.searchqueryset = kwargs.pop('searchqueryset', None)
        self.load_all = kwargs.pop('load_all', False)

        if self.searchqueryset is None:
            self.searchqueryset = SearchQuerySet()

        super(FilterForm, self).__init__(*args, **kwargs)

    def search(self):
        # First, store the SearchQuerySet received from other processing.
        if not self.is_valid():
            return self.no_query_found()

        if not self.is_valid():
            return self.no_query_found()
        sqs = self.searchqueryset.all()
        for field_name in ['ram', 'os', 'memory', 'display', 'camera']:
            if self.cleaned_data[field_name]:
                kwargs = {'{}__{}'.format(field_name, "in"): self.cleaned_data[field_name]}
                sqs = sqs.filter(**kwargs)

        if self.cleaned_data['price'] is not None:
            sqs = sqs.filter(price__lte=self.cleaned_data['price'])

        if self.cleaned_data['gps']:
            sqs = sqs.filter(gps=self.cleaned_data['gps'])

        if self.cleaned_data['name']:
            sqs = sqs.auto_query(self.cleaned_data['name'])

        return sqs


    def no_query_found(self):
        """
        Determines the behavior when no query was found.

        By default, no results are returned (``EmptySearchQuerySet``).

        Should you want to show all results, override this method in your
        own ``SearchForm`` subclass and do ``return self.searchqueryset.all()``.
        """
        return EmptySearchQuerySet()
