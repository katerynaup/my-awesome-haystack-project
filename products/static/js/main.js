$(function () {
    $('#id_name').autocomplete({
        source: function (request, callback) {
        $.getJSON("/autocomplete_phone/?phone=" + request.term, function (data) {
            callback(data['results']);
        });
    }
    });
    function showSelectedPrice() {
        var currentPrice = $('#id_price').val();
        $('#show-price-input').val(currentPrice);

    }
    showSelectedPrice();
    $('#id_price').change(showSelectedPrice)

})
