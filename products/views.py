from django.http.response import JsonResponse
from haystack.generic_views import SearchView
from haystack.query import SearchQuerySet

from products.forms import FilterForm
from products.models import Phone


class PhoneListView(SearchView):
    """My custom search view."""
    form_class = FilterForm
    template_name = 'products/phone_list.html'



def autocomplete_phone(request):
    auto = SearchQuerySet().models(Phone).autocomplete(auto=request.GET.get('phone', ''))
    suggestions = [result.name for result in auto]
    data = {
        'results': suggestions
    }
    return JsonResponse(data)


